import sun.lwawt.PlatformEventNotifier

import scala.annotation.tailrec

//definitions and val variables
def calledWhenNeeded = 2

val evaluatedImmediately = 2

lazy val evaluatedWhenNeeded = 2

//Functions parameters call by name and call by value
def func(): Int = {
  println("EvaluatingFunction")
  100
}

def callByName(x: => Int) = {
  println("x = " + x)
  println("x = " + x)
}

def callByValue(x: Int) = {
  println("x = " + x)
  println("x = " + x)
}

def varArgs(x: Int*) = {
  print(x)
}

callByName(func)
callByValue(func())
varArgs(1, 2, 3, 4)

//Higher order functions that return functions
def func(x: Int): Int = {
  x * 2
}

def sum(f: Int => Int): (Int, Int) => Int = {
  def sumNew(x: Int, y: Int): Int = {
    f(x) + f(y)
  }
  sumNew
}

sum(func)(1, 3)
sum((a) => a * a)(2, 3)

//What is currying
def unCurriedMinus(x: Int, y: Int) = {
  x - y
}

def curriedMinus(x: Int)(y: Int) = {
  x - y
}

curriedMinus(10)(30)

//Classes in Scala
class MyClass(x: Int, y: Int) {
  require(x > 0)
  require(y > 0)

  //Auxilary Constructor
  def this(x: Int) = {
    this(x, 20)
  }

  private val sum = x + y //This value is evaluated at object creation level

  def getX = x
  def getY = y

  private def cannotCallFromOutside() = {
    "You will never get this"
  }

  override def toString: String = {
    x + "," + y
  }
}

val myClassObject = new MyClass(10, 20)
println(myClassObject.getX)
println(myClassObject.getY)
println(myClassObject.toString)

//Inheritance
abstract class TopLevel {
  def abstractMethod(): Int
  def method(): Int = {
    10
  }
}

class Level1 extends TopLevel {
  def abstractMethod(): Int = {
    100
  }

  override def method(): Int = {
    20
  }
}

object SingletonObject extends TopLevel {
  override def abstractMethod(): Int = 10
}

//Base classes
val x: scala.AnyRef = new String("karthik")

//Generic
class Generic[T](x: T) {
  def getX = {
    x
  }
}

val generic = new Generic[Int](10)
println(generic.getX)

val genericWithoutType = new Generic(10)
println(generic.getX)

class Parent {
  def parent = 10
}

class Child1 extends Parent {
  def child1 = 20
}

class Child2 extends Child1 {
  def child2 = 30
}

def funcChilds[T <: Parent](x: T): T = {
  x
}

def funcParents[T >: Child2](x: T): T = {
  x
}

//CoVariance and ContraVariance
class Drink {
}

class Juice extends Drink {
}

class OrangeJuice extends Juice {
}

class AppleJuice extends Juice {
}

class SoftDrink extends Drink {
}

class Cola extends SoftDrink {
}

class Tonic extends SoftDrink {
}

class VendingMachine[+A] {
}

def install(v: VendingMachine[SoftDrink]): Unit = {
}

install(new VendingMachine[Cola])
install(new VendingMachine[Tonic])
install(new VendingMachine[SoftDrink])
//install(new VendingMachine[Drink]) //compilation error

class Item {
}

class PlasticItem extends Item {
}

class PlasticBottle extends PlasticItem {
}

class PaperItem extends Item {
}

class NewsPaper extends PaperItem {
}

class GarbageCan[-A] {
}

def setGarbageCanForPlastic(gc: GarbageCan[PlasticItem]) {
}

setGarbageCanForPlastic(new GarbageCan[Item])
setGarbageCanForPlastic(new GarbageCan[PlasticItem])
//setGarbageCanForPlastic(new GarbageCan[PlasticBottle]) //compilation error

//Matching Pattern
def matchInts(x: Int) = x match {
    case 1 => "one"
    case 2 => "two"
    case 3 => "three"
    case _ => "AnythingElse"
}

matchInts(3)

def matchAny(x: Any) = x match {
  case 1 => "one"
  case x: Int => "It is Int Type"
  case "two" => 2
  case _ => "OtherType"
}

matchAny("two")
matchAny(20)
matchAny(2.0)
matchAny(1)

def matchList[T](x: List[T]) = x match {
  case Nil => "list is empty"
  case (x,y)::Nil => "Has only a pair"
  case (x,y)::p => "starts with a pair"
  case 1::a => "starts with one"
  case _ => "Other list"
}

matchList(List(1, "two"))
matchList(List((1,2), 3, 4))
matchList(List((1,2)))

//Matching map
val map = Map("one" -> 1, "two" -> 2)
def matchMap(x: String): String = {
  map.get(x) match {
    case Some(x) => "value found"
    case None => "value not found"
  }
}

println(matchMap("one"))
println(matchMap("hundred"))

def mapLookup(x: String): String = {
  map.get(x).map("Value found" + _).getOrElse("No Value found")
}

mapLookup("one")
mapLookup("hundred")

//Collections
val fruits = List("Apples", "Oranges", "Pears")
val fruit = "Apples" :: "Oranges" :: "Pears" :: Nil

fruits.head
fruit.tail

val emptyList = List()
val emptyListNil = Nil

val vector = Vector("Louis", "Frank", "Haromi")
vector(1)
vector.updated(2, "ChangedHaromi")

val fruitSet = Set("Bananas", "Apples", "Bananas", "Pears")
fruitSet.size

val range = 5.until(10)
val rangeTo = 1.to(10)
val rangeBy = 1 to 10 by 3
val rangeLessBy = 6 to 1 by -2

val rangeSet = rangeLessBy.toSet
rangeSet.map(s => s + 2)

val s = "Hello World"
s.filter(s => s.isUpper)

val seq = List("K", "L", "M")
seq.length
seq.last
seq.init

seq.take(2)
seq.drop(2)
seq(2)
seq ++ seq
seq.reverse
seq.updated(1, "B")
seq.indexOf("L")
seq.contains("L")
seq.filter(rec => rec.equals("K") || rec.equals("L"))
seq.filterNot(rec => rec.equals("K"))
seq.partition(rec => rec.equals("L"))
List(1, 2, 3, 4).takeWhile(rec => rec < 3)
List(1, 2, 3, 4).dropWhile(rec => rec < 3)
List(1, 2, 3, 4).span(rec => rec < 3)

List("A", "B", "c", "d").reduceLeft((a, b) => a + b)
List("A", "B", "c", "d").foldLeft("Z")((a, b) => a + b)
List("A", "B", "c", "d").reduceRight((a, b) => a + b)
List("A", "B", "c", "d").foldRight("Z")((a, b) => a + b)

seq.exists(_.equals("L"))
List(1, 2, 3, 4).forall(_ < 5)

val pairList = seq zip List(1, 2, 3, 4)
val unzippedList = pairList.unzip
seq.flatMap(rec => List(1, 2) ++ rec)

val intList = List(1, 2, 3, 4)
intList.sum
intList.product
intList.min
intList.max

val listOfList = List(List(1, 2, 3, 4, List(1, 2, 3, 4)), List("A", "B", "C", "D"))
listOfList.flatten

seq.groupBy(rec => rec)
seq :+ 'C'
'C' +: seq

val newMap = Map('V' -> 5, 'I' -> 10, 'L' -> 20)
newMap('V')
//newMap('A') //Throws exception
newMap.get('A')
newMap.get('V')

val xs = Stream(1, 2, 3)
(1 to 1000).toStream

val pair = ("answer", 42)
val (label, value) = pair

pair._1
pair._2

//For loop
for (x <- 1 to 10; y <- 1 to 10)
  yield(x, y)

(1 to 10).map(x => (1 to 10).map(y => (x, y)))

for(
  x <- 1 to 10;
  y <- 1 to 10
  if (x + y) % 2 == 0
) yield (x, y)

//Fibonocci
def fibonocciMutable(n: Int): Int = {
  var first: Int = 0
  var next: Int = 1
  (1 to n).map(a => {
    val buf = first
    first = next
    next = buf + next
  })
  next
}

fibonocciMutable(4)

def fibonocci(n: Int): Int = {
  def fibIter(a: Int, b: Int, c: Int): Int = {
    if (a == n) {
      c
    }
    else {
      fibIter(a + 1, c, b + c)
    }
  }
  fibIter(0, 0, 1)
}

fibonocci(4)

//And or operations
def and(x: Boolean, y: => Boolean): Boolean = {
  if (!x) {
    false
  }
  else {
    y
  }
}

def loop(): Boolean = {
  loop()
}

def or(x: Boolean, y: => Boolean): Boolean = {
  if (x) {
    true
  }
  else {
    y
  }
}

and(true, true)
and(true, false)
and(false, true)
and(false, false)
and(false, loop())

or(true, loop())
or(true, true)
or(true, false)
or(false, true)
or(false, false)

//Sqrt
def sqrt(x: Double): Double = {
  @tailrec
  def sqrtIter(seed: Double) : Double = {
    if (isGoodEnough(seed)) {
      seed
    }
    else {
      val divide = x / seed
      val mean = (seed + divide) / 2
      sqrtIter(mean)
    }
  }
  def isGoodEnough(seed: Double) = {
    Math.abs(seed * seed - x)/ x <= 0.01
  }
  sqrtIter(1)
}

sqrt(2)
sqrt(4)
sqrt(1e60)
sqrt(1e-6)

//Factorial TailRec
def factorial(n: Int): Int = {
  @tailrec
  def factIter(acc: Int, n:Int): Int = {
    if (n == 1) {
      acc
    }
    else {
      factIter(acc * n, n - 1)
    }
  }
  factIter(1, n)
}

factorial(4)

//get pascal number
def pascal(col: Int, row: Int): Int = {
  if (col < 0) {
    0
  }
  else if (row == 0 && col == 0) {
    1
  }
  else if (col > row) {
    0
  }
  else {
    pascal(col - 1, row - 1) + pascal(col, row - 1)
  }
}

pascal(0, 2)
pascal(1, 2)
pascal(1, 3)

//is balanced
def isBalanced(chars: List[Char]) = {
  @tailrec
  def isBalancedIter(counter: Int, position: Int): Boolean = {
    if (counter < 0) {
      false
    }
    else if (position == chars.length && counter == 0) {
      true
    }
    else if (position == chars.length && counter != 0) {
      false
    }
    else if (chars(position) == '(') {
      isBalancedIter(counter + 1, position + 1)
    }
    else if (chars(position) == ')') {
      isBalancedIter(counter - 1, position + 1)
    }
    else {
      isBalancedIter(counter, position + 1)
    }
  }
  isBalancedIter(0, 0)
}

isBalanced("(if (zero? x) max (/ 1 x))".toList)
isBalanced("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList)
isBalanced(":-)".toList)
isBalanced("())(".toList)

//Counting change
def countChange(money: Int, coins: List[Int]): Int = {
  val sortedCoins = coins.sortWith((a, b) => a < b)

  if (coins.filter(a => money < a).length == coins.length) {
    0
  }
  else {
    def countIter(acc: Int, maxPos: Int): Int = {
      if (acc == money) {
        1
      }
      else if (acc > money) {
        0
      }
      else {
        (for {
          i <- maxPos until sortedCoins.length
        } yield countIter(acc + sortedCoins(i), i)).sum
      }
    }
    countIter(0, 0)
  }
}

countChange(4, List(1, 2))

//Higher order functions
def sum(f: Int => Int, a: Int, b: Int): Int = {
  def sumIter(acc: Int, value: Int): Int = {
    if (value > b) {
      acc
    }
    else {
      sumIter(acc + f(value), value + 1)
    }
  }
  sumIter(0, a)
}

def sumInts(a: Int, b: Int): Int = {
  sum(a => a, a, b)
}

sumInts(1, 3)

def cube(a: Int) = {
  a * a * a
}

def sumCubes(a: Int, b: Int): Int = {
  sum(cube, a, b)
}

def sumFactorials(a: Int, b: Int): Int = {
  sum(factorial, a, b)
}

//Function returning a function
def sumCurry(f: Int => Int): (Int, Int) => Int = {
  def sumF(a: Int, b: Int): Int = {
    if (a > b) {
      0
    }
    else {
      f(a) + sumF(a + 1, b)
    }
  }
  sumF
}

sumCurry(cube)(1, 10)

def sumRealCurry(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 0 else f(a) + sumRealCurry(f)(a + 1, b)
}

sumCurry(cube)(1, 10)

def productCurry(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 1
  else
    f(a) * productCurry(f)(a + 1, b)
}

productCurry(x => x * x)(3, 7)

def genericFunctionCurry(reduce: (Int, Int) => Int, map: Int => Int, zero: Int)(a: Int, b: Int): Int = {
  if (a > b) 1
  else
    reduce(map(a), genericFunctionCurry(reduce, map, zero)(a + 1, b))
}

def product(f: Int => Int)(a: Int, b: Int) = genericFunctionCurry((a, b) => a * b, f, 1)(a, b)

//Fixed Point
def fixedPoint(f: Double => Double)(guess: Double) = {
  val tolerance = 0.0001
  def guessCloseEnough(x: Double, y: Double): Boolean = {
    Math.abs((x - y)/x)/x < tolerance
  }

  def iterate(guess: Double): Double = {
    val next = f(guess)
    if (guessCloseEnough(guess, f(guess)))
      guess
    else
      iterate(f(guess))
  }
  iterate(guess)
}

fixedPoint(x => 1 + x/2)(1)

def averageDamp(f: Double => Double)(x: Double) = (x + f(x))/2

def sqrt(x: Int) = fixedPoint(averageDamp(y => x / y))(1.0)
sqrt(4)

//Rational Functions
class Rational(x: Int, y: Int) {
  def this(x: Int) = this(x, 1)

  require(y != 0, "Denominator must be non-zero")
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  val g = gcd(x, y);
  def numer = x / g
  def denom = y / g

  def +(that: Rational) = {
    new Rational(numer * that.denom + that.numer * denom, denom * that.denom)
  }

  def -(that: Rational) = {
    this + -that
  }

  def unary_- = {
    new Rational(-numer, denom)
  }

  def < (that: Rational) = this.numer * that.denom < that.numer * this.denom

  def max(that: Rational) = if (this < that) that else this

  override def toString = numer + "/" + denom
}

val xRational = new Rational(1, 3)
val yRational = new Rational(5, 7)
val zRational = new Rational(3, 2)

xRational + yRational
yRational + yRational
xRational max yRational
xRational < yRational

xRational - yRational - zRational

//Abstract classes
abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
  def union(that: IntSet): IntSet
}

//Considering this as object creates a singleton object
object Empty extends IntSet {
  override def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
  override def contains(x: Int): Boolean = false
  override def toString(): String = "."
  override def union(that: IntSet): IntSet = that
}

class NonEmpty(element: Int, left: IntSet, right: IntSet) extends IntSet {
  override def contains(x: Int): Boolean = {
    if (x > element) right.contains(x)
    else if (x < element) left.contains(x)
    else true
  }

  override def incl(x: Int): IntSet = {
    if (x < element) new NonEmpty(element, left.incl(x), right)
    else if (x > element) new NonEmpty(element, left, right.incl(x))
    else this
  }

  //Here include is the key
  override def union(that: IntSet): IntSet = {
    this.left union that union this.right incl this.element
  }

  override def toString(): String = "{" + left + element + right + "}"
}

val three4 = new NonEmpty(3, Empty, Empty)
val three4Final = three4.incl(4)

val six7 = new NonEmpty(6, Empty, Empty)
val six7Final = six7.incl(7)

three4Final.union(six7Final)

//Override
abstract class Base {
  def foo: Int = 1
  def bar: Int
}

class Sub extends Base {
  override def foo: Int = 2
  def bar: Int = 5
}

//Traits are abstract classes types that can be enable multiple inheritance
trait Planar {
  def height: Int
  def width: Int
  def surface: Int = height * width
}

abstract class Shape {
}

class Rectangle (h: Int, w: Int) extends Shape with Planar {
  override def height: Int = this.h
  override def width: Int = this.w
}

//Any, AnyVal (Primitives), AnyRef (Object),
//Nothing (Return type of exceptions or empty sets, list etc.,)
//Null is a subtype of every class AnyRef not AnyVal. null is an object of Null
def error(s: String): Nothing = throw new Error(s)
//error("Test")
val nullValue = null
//val intValue: Int = null
if (true) 1 else false

//Polymorphism
//with the introduction of type, a data type can be of multiple types
val listNum = List(1, 2, 3)
val listOfListObject = List(List(true, false), List(1, 2, 3))

trait ListA[T] {
  def isEmpty: Boolean
  def head: T
  def tail: ListA[T]
}
class ConsA[T](val head: T, val tail: ListA[T]) extends ListA[T] {
  override def isEmpty: Boolean = false
}
class NilA[T] extends ListA[T] {
  override def isEmpty: Boolean = true
  override def head: Nothing = throw new NoSuchElementException("Nil.head")
  override def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}

def singleton[T](a: T) = new ConsA[T](a, new NilA[T])
singleton(1)
singleton(true)

@tailrec
def nth[T](pos: Int, list: ListA[T]): T = {
  if (pos < 0) throw new IllegalArgumentException("Position Cannot be negaive")
  else if (list.isEmpty) throw new IndexOutOfBoundsException("Nil")
  else if (pos == 0) list.head
  else nth(pos - 1, list.tail)
}

val listA = new ConsA[Int](1, new ConsA[Int](2, new ConsA[Int](3, new NilA[Int])))

//nth(-10, listA)
nth(0, listA)
nth(1, listA)
nth(2, listA)
//nth(3, listA)
//nth(20, listA)
